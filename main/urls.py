from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('other/', views.other, name='other'),
    path('story1/', views.story1, name='story1')
]
