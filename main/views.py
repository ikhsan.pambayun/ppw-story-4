from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def other(request):
    return render(request, 'main/other.html')

def story1(request):
    return render(request, 'main/story1.html')
